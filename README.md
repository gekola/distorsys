distorsys
=========

distorsys is an yet another distributed storage system for MS Windows.

It contains 3 modules:

  1. distorsys-driver (fs write event notifier)
  2. distorser (files tree constistency keeper)
  3. distorsysc (simple virtual fs navigatior GUI)
