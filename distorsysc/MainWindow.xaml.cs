﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace distorsysc {
  public partial class MainWindow : Window {
    public MainWindow() {
      InitializeComponent();
    }

    private void ExitClick(object sender, RoutedEventArgs e) {
      Application.Current.Shutdown();
    }

    private void UpdateClick(object sender, RoutedEventArgs e) {
      using (var pipe = new NamedPipeClientStream("distorser_pipe"))
      {
        try {
          pipe.Connect(1000);
        } catch (TimeoutException) {
          MessageBox.Show("Sorry, cannot connect to the service.\nCheck if it is running.",
                          "Connection Timeout.",
                          MessageBoxButton.OK,
                          MessageBoxImage.Error);
          return;
        }
        string path, prefix;
        pipe.ReadMode = PipeTransmissionMode.Message;

        var sw = new StreamWriter(pipe, Encoding.Unicode);

        var data = Encoding.Unicode.GetBytes(@"\");
        pipe.Write(data, 0, data.Length);
        pipe.Flush();

        var sr = new StreamReader(pipe, Encoding.Unicode);
        items.Items.Clear();
        while ((path = sr.ReadLine()) != null) {
          if (path == "")
            break;

          prefix = sr.ReadLine();

          if (prefix == null)
            break;

          items.Items.Add(new DistFile(path, prefix));
        }
        sw.Dispose();
      }
    }

    private void ItemDoubleClick(object sender, RoutedEventArgs e) {
      var file = ((FrameworkElement)e.OriginalSource).DataContext as DistFile;
      if (null != file)
        file.Open();
    }
  }
}
