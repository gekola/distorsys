﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace distorsysc
{
  class DistFile {
    public DistFile(string path, string prefix) {
      Path = path;
      Prefix = prefix;
    }

    public void Open() {
      Process.Start("explorer.exe", "/select, " + Prefix + Path);
    }

    public string Path { get; set; }
    public string Prefix { get; set; }
  }
}
