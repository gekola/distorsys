#ifndef __DEVICE_H__
#define __DEVICE_H__

#ifndef MY_DRIVER_NAME
#define MY_DRIVER_NAME L"distorsys_fs_filter"
#endif

#define DISTORSYS_DEV_NAME (L"\\Device\\" MY_DRIVER_NAME)
#define DISTORSYS_DEV_SYM_NAME (L"\\DosDevices\\" MY_DRIVER_NAME)

#endif
