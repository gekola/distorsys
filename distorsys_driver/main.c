#include "main.h"

/******************************************************************************
 *  Globals and constants
 ******************************************************************************/
PDEVICE_OBJECT g_dev_obj;
PDRIVER_OBJECT g_driver_object;
PFLT_FILTER g_filter;
KMUTEX g_mutex;

INT g_stack_top = 0;
PUNICODE_STRING g_stack[GSTACK_SIZE];
UNICODE_STRING g_prefix = {0, 0, NULL};

const FLT_OPERATION_REGISTRATION g_callbacks[] = {
  {IRP_MJ_WRITE, 0, NULL, post_file_op_clb},

  {IRP_MJ_OPERATION_END}
};

const FLT_REGISTRATION g_filter_reg = {
  sizeof(FLT_REGISTRATION),
  FLT_REGISTRATION_VERSION,
  0,                          //  Flags

  NULL,                       //  Context
  g_callbacks,                //  Callbacks

  filter_unload,
  filter_load,

  NULL,                       //  InstanceQueryTeardown
  NULL,                       //  InstanceTeardownStart
  NULL,                       //  InstanceTeardownComplete

  NULL,                       //  GenerateFileName
  NULL                        //  NormalizeNameComponent
};

/******************************************************************************
 *  Driver entry
 ******************************************************************************/
NTSTATUS driver_entry(PDRIVER_OBJECT driver_obj, PUNICODE_STRING reg_path)
{
  int i;
  PDEVICE_OBJECT dev_obj;
  UNICODE_STRING dev_name, dev_sym_name;
  NTSTATUS status;

  UNREFERENCED_PARAMETER(reg_path);

  DDbgPrint("Loading...\r\n");

  for (i = 0; i < IRP_MJ_MAXIMUM_FUNCTION; ++i) {
    driver_obj->MajorFunction[i] = stub_dispatch;
  }

  driver_obj->MajorFunction[IRP_MJ_DEVICE_CONTROL] = dispatch_dev_ctrl;
  driver_obj->MajorFunction[IRP_MJ_CREATE] = dispatch_dev_create;

  driver_obj->DriverUnload = driver_unload;

  g_driver_object = driver_obj;

  RtlInitUnicodeString(&dev_name, DISTORSYS_DEV_NAME);
  RtlInitUnicodeString(&dev_sym_name, DISTORSYS_DEV_SYM_NAME);
  // RtlInitUnicodeString(&g_prefix, L"\\Device\\HarddiskVolume2\\temp\\");

  status = IoCreateDevice(driver_obj, 0, &dev_name, FILE_DEVICE_UNKNOWN,
                          0, FALSE, &dev_obj);

  if (!NT_SUCCESS(status)) {
    DDbgPrint("Driver not started. ERROR IoCreateDevice - %08x\r\n", status);
    return status;
  }

  dev_obj->Flags &= ~DO_DEVICE_INITIALIZING;
  g_dev_obj = dev_obj;

  //  Create user-visible name for the device
  status = IoCreateSymbolicLink(&dev_sym_name, &dev_name);
  if (!NT_SUCCESS(status)) {
    DDbgPrint("Driver not started. ERROR IoCreateSymbolicLink - %08x\r\n", status);
    IoDeleteDevice(driver_obj->DeviceObject);
    return status;
  }


  status = FltRegisterFilter(driver_obj, &g_filter_reg, &g_filter);

  if (!NT_SUCCESS(status)) {
    DDbgPrint("Driver not started. ERROR FltRegisterFilter - %08x\r\n", status);

    IoDeleteSymbolicLink(&dev_sym_name);
    IoDeleteDevice(driver_obj->DeviceObject);
    return status;
  }

  status = FltStartFiltering(g_filter);

  if (!NT_SUCCESS(status)) {
    DDbgPrint("Driver not started. ERROR FltStartFiltering - %08x\r\n", status);

    IoDeleteSymbolicLink(&dev_sym_name);
    IoDeleteDevice(driver_obj->DeviceObject);
    FltUnregisterFilter(g_filter);
    return status;
  }

  KeInitializeMutex(&g_mutex, 0);
  // KeReleaseMutex(&g_mutex, FALSE);

  DDbgPrint("Filter was started and configured.\r\n");

  return STATUS_SUCCESS;
}

/******************************************************************************
 *  Driver unload routine
 ******************************************************************************/
VOID driver_unload(PDRIVER_OBJECT driver_obj)
{
  UNICODE_STRING dev_name, dev_sym_name;
#ifndef RELEASE_VERSION
  ULONG          num_devices = 0;
  ULONG          i           = 0;
  LARGE_INTEGER  interval;
  PDEVICE_OBJECT dev_list[DEV_LIST_SIZE];
#endif

  //  Unregistered callback routine for file system changes.
  FltUnregisterFilter(g_filter);

  RtlInitUnicodeString(&dev_name, DISTORSYS_DEV_NAME);
  RtlInitUnicodeString(&dev_sym_name, DISTORSYS_DEV_SYM_NAME);
  IoDeleteSymbolicLink(&dev_sym_name);
  IoDeleteDevice(driver_obj->DeviceObject);

#ifndef RELEASE_VERSION
  //  This is the loop that will go through all of the devices we are attached
  //  to and detach from them.
  interval.QuadPart = -50000000ll; //delay 5 seconds
  for (;;) {
    IoEnumerateDeviceObjectList(driver_obj,
                                dev_list,
                                sizeof(dev_list),
                                &num_devices);

    if (0 == num_devices)
      break;

    num_devices = min(num_devices, RTL_NUMBER_OF(dev_list));

    for (i = 0; i < num_devices; ++i) {
      PDEVICE_OBJECT *p_dev_ext =
        (PDEVICE_OBJECT *)dev_list[i]->DeviceExtension;

      IoDetachDevice(*p_dev_ext);
      IoDeleteDevice(dev_list[i]);

      ObDereferenceObject(dev_list[i]);
    }
    KeDelayExecutionThread(KernelMode, FALSE, &interval);
  }
#endif

  DDbgPrint("Driver unloaded.\r\n");
}


/******************************************************************************
 *  Workitem routine adding file name to notification list
 ******************************************************************************/
VOID add_file(PDEVICE_OBJECT dev_obj, PVOID context)
{
  NTSTATUS status;
  PMY_WI_PARAM param = (PMY_WI_PARAM) context;

  UNREFERENCED_PARAMETER(dev_obj);

  IoFreeWorkItem(param->workitem);

  do
    status = KeWaitForSingleObject(&g_mutex, Executive, KernelMode, FALSE, NULL);
  while (status != STATUS_SUCCESS && status != STATUS_ABANDONED_WAIT_0);

  if (g_prefix.Length < param->filename->Length &&
      wcsncmp(g_prefix.Buffer,
              param->filename->Buffer,
              g_prefix.Length / sizeof(wchar_t)) == 0) {
    DDbgPrint("Adding file: (%d) %ws\r\n", param->filename->Length, param->filename->Buffer);
    DDbgPrint("Stack depth: %d\r\n", g_stack_top);

    if (g_stack_top < GSTACK_SIZE) {
      g_stack[g_stack_top++] = param->filename;
    } else {
      // Handle possible miss.
      DDbgPrint("Sorry, stack is full\r\n");
      ExFreePool(param->filename);
    }
  } else {
    ExFreePool(param->filename);
  }

  KeReleaseMutex(&g_mutex, FALSE);

  ExFreePool(param);
}
