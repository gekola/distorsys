#include "main.h"

/******************************************************************************
 *  Filter load routine (attach to all local drives)
 ******************************************************************************/
NTSTATUS filter_load(PCFLT_RELATED_OBJECTS flt_objs,
                     FLT_INSTANCE_SETUP_FLAGS flags,
                     DEVICE_TYPE vol_dev_type,
                     FLT_FILESYSTEM_TYPE vol_fs_type)
{
  UNREFERENCED_PARAMETER(flt_objs);
  UNREFERENCED_PARAMETER(flags);
  UNREFERENCED_PARAMETER(vol_fs_type);

  if (vol_dev_type == FILE_DEVICE_NETWORK_FILE_SYSTEM)
    return STATUS_FLT_DO_NOT_ATTACH;

  return STATUS_SUCCESS;
}

/******************************************************************************
 *  Filter unload routine (dummy)
 ******************************************************************************/
NTSTATUS filter_unload(FLT_FILTER_UNLOAD_FLAGS flags)
{
  UNREFERENCED_PARAMETER(flags);

  return STATUS_SUCCESS;
}


/******************************************************************************
 *  Post file operation callback
 ******************************************************************************/
FLT_POSTOP_CALLBACK_STATUS
post_file_op_clb(PFLT_CALLBACK_DATA data,
                 PCFLT_RELATED_OBJECTS flt_objs,
                 PVOID *completion_context,
                 FLT_POST_OPERATION_FLAGS flags)
{
  NTSTATUS status;
  PFILE_OBJECT file_obj;
  PFLT_FILE_NAME_INFORMATION file_name_info;
  PMY_WI_PARAM wi_param = NULL;

  UNREFERENCED_PARAMETER(completion_context);
  UNREFERENCED_PARAMETER(flags);

  if (FLT_IS_FS_FILTER_OPERATION(data) || !flt_objs->FileObject || !data)
    return FLT_POSTOP_FINISHED_PROCESSING;

  file_obj = data->Iopb->TargetFileObject;
  if (!file_obj || data->Iopb->MajorFunction != IRP_MJ_WRITE)
    return FLT_POSTOP_FINISHED_PROCESSING;

  if (g_prefix.Length == 0)
    return FLT_POSTOP_FINISHED_PROCESSING;

  status = FltGetFileNameInformation(data,
                                     FLT_FILE_NAME_NORMALIZED |
                                     FLT_FILE_NAME_QUERY_ALWAYS_ALLOW_CACHE_LOOKUP,
                                     &file_name_info);
  if (!NT_SUCCESS(status))
    return FLT_POSTOP_FINISHED_PROCESSING;

  if (!g_prefix.Buffer || g_prefix.Length == 0)
    return FLT_POSTOP_FINISHED_PROCESSING;
 
  wi_param = ExAllocatePoolWithTag(NonPagedPool, sizeof(MY_WI_PARAM), 'dWIp');
  if (wi_param) {
    PVOID buf = NULL;
    buf = ExAllocatePoolWithTag(NonPagedPool,
                                sizeof(UNICODE_STRING) +
                                file_name_info->Name.Length,
                                'dUfn');
    wi_param->filename = (PUNICODE_STRING) buf;
    wi_param->workitem = IoAllocateWorkItem(g_dev_obj);
  }

  if (!wi_param || !wi_param->workitem || !wi_param->filename) {
    // TODO: Fix possible miss
    DDbgPrint("Cannot allocate workitem arguments.\r\n");

    // Cleanup
    if (wi_param) {
      if (wi_param->workitem) {
        IoFreeWorkItem(wi_param->workitem);
      }

      if (wi_param->filename) {
        ExFreePool(wi_param->filename);
      }

      ExFreePool(wi_param);
    }
    return FLT_POSTOP_FINISHED_PROCESSING;
  }

  wi_param->filename->Buffer = (wchar_t *)(wi_param->filename + 1);
  wi_param->filename->MaximumLength =
    wi_param->filename->Length = file_name_info->Name.Length;

  wcsncpy(wi_param->filename->Buffer, file_name_info->Name.Buffer,
          file_name_info->Name.Length / sizeof(wchar_t));

  IoQueueWorkItem(wi_param->workitem, add_file, DelayedWorkQueue, wi_param);

  //DDbgPrint("File name: %ws\r\n", file_name_info->Name.Buffer);

  return FLT_POSTOP_FINISHED_PROCESSING;
}
