#ifndef __IOCTL_H__
#define __IOCTL_H__

#include <devioctl.h>

#define D_CTL CTL_CODE(FILE_DEVICE_UNKNOWN, 0x810, \
                       METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef enum _MyIoCode {
  // MCODE_QUIT = 0,
  MCODE_SET_PREF = 1,
  MCODE_GET = 2,

  MCODE_TEST = 42
} MyIoCode;

typedef __declspec(align(8))
struct _IOCTL_DATA {
  MyIoCode code;
  unsigned short sz;
  wchar_t *wstr;
} IOCTL_DATA, *PIOCTL_DATA;

#endif
