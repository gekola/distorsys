#include "main.h"

/******************************************************************************
 *  Stup dispatch routine
 ******************************************************************************/
NTSTATUS stub_dispatch(PDEVICE_OBJECT device_obj, PIRP irp)
{
  UNREFERENCED_PARAMETER(device_obj);

  irp->IoStatus.Status = STATUS_SUCCESS;
  IoCompleteRequest(irp, IO_NO_INCREMENT);
  return irp->IoStatus.Status;
}

/******************************************************************************
 *  Device open handler
 ******************************************************************************/
NTSTATUS dispatch_dev_create(PDEVICE_OBJECT device_obj, PIRP irp)
{
  UNREFERENCED_PARAMETER(device_obj);

  DDbgPrint("Somebody opened handler!\r\n");

  irp->IoStatus.Status = STATUS_SUCCESS;
  irp->IoStatus.Information = 0;
  IoCompleteRequest(irp, IO_NO_INCREMENT);

  DDbgPrint("Request is completed.\r\n");

  return irp->IoStatus.Status;
}

/******************************************************************************
 *  Device messages handler
 ******************************************************************************/
#define EXIT_PROC(status) {                \
  irp->IoStatus.Status = status;           \
  IoCompleteRequest(irp, IO_NO_INCREMENT); \
  return irp->IoStatus.Status;             \
}
NTSTATUS dispatch_dev_ctrl(PDEVICE_OBJECT device_obj, PIRP irp)
{
  PIO_STACK_LOCATION io_stack;

  UNREFERENCED_PARAMETER(device_obj);

  irp->IoStatus.Status = STATUS_NOT_IMPLEMENTED;
  irp->IoStatus.Information = 0; // answer length

  io_stack = IoGetCurrentIrpStackLocation(irp);
  if (!io_stack)
    EXIT_PROC(STATUS_INTERNAL_ERROR);

  if (io_stack->MajorFunction != IRP_MJ_DEVICE_CONTROL)
    EXIT_PROC(STATUS_INTERNAL_ERROR);

  switch (io_stack->Parameters.DeviceIoControl.IoControlCode) {
  case D_CTL: {
    PIOCTL_DATA data = (PIOCTL_DATA) irp->AssociatedIrp.SystemBuffer;
    size_t real_len;
    USHORT len;
    NTSTATUS status;

    DDbgPrint("size: %d / %d, pointer: %x\r\n",
              io_stack->Parameters.DeviceIoControl.InputBufferLength,
              sizeof(IOCTL_DATA),
              data);

    if ((io_stack->Parameters.DeviceIoControl.InputBufferLength <
         sizeof(IOCTL_DATA)) ||
        (io_stack->Parameters.DeviceIoControl.OutputBufferLength <
         sizeof(IOCTL_DATA)) ||
        !data)
      EXIT_PROC(STATUS_INVALID_PARAMETER);

    switch (data->code) {
    case MCODE_TEST:
      DDbgPrint("ioctl(IOCTL_EXAMPLE) called, arg = %d\r\n", data->code);
      break;

    case MCODE_SET_PREF:
      real_len = wcslen(data->wstr);
      if (real_len <= MAXUSHORT/sizeof(wchar_t)) {
        len = (USHORT) real_len;
      } else {
        // TODO: set response error
        break;
      }

      do
        status = KeWaitForSingleObject(&g_mutex, UserRequest, KernelMode, FALSE, NULL);
      while (status != STATUS_SUCCESS && status != STATUS_ABANDONED_WAIT_0);

      if (g_prefix.Buffer && g_prefix.MaximumLength <= len * sizeof(wchar_t)) {
        DDbgPrint("Freeing...\r\n");
        ExFreePool(g_prefix.Buffer);
      }
      if (!g_prefix.Buffer) {
        USHORT size = (1 + len) * sizeof(wchar_t);
        DDbgPrint("Allocating...\r\n");
        g_prefix.Buffer = ExAllocatePoolWithTag(NonPagedPool, size, 'dpre');
        g_prefix.MaximumLength = size;
      }
      if (g_prefix.Buffer) {
        DDbgPrint("New prefix: %ws\r\n", data->wstr);
        wcscpy(g_prefix.Buffer, data->wstr);
        g_prefix.Buffer[len] = L'\0';
        g_prefix.Length = len * sizeof(wchar_t);
      } else {
        DDbgPrint("Cannot allocate resources for new prefix.\r\n");
        // TODO: Set response error.
        g_prefix.MaximumLength = g_prefix.Length = 0;
      }
      KeReleaseMutex(&g_mutex, FALSE);
      break;

    case MCODE_GET:
      irp->IoStatus.Information = sizeof(IOCTL_DATA);

      do
        status = KeWaitForSingleObject(&g_mutex, Executive, KernelMode, FALSE, NULL);
      while (status != STATUS_SUCCESS && status != STATUS_ABANDONED_WAIT_0);

      if (g_stack_top == 0) {
        KeReleaseMutex(&g_mutex, FALSE);
        DDbgPrint("Nothing to pop!\r\n");
        data->sz = 0;
        break;
      }

      --g_stack_top;
      len = g_stack[g_stack_top]->Length / sizeof(wchar_t);
      if (data->sz > len) {
        wcsncpy(data->wstr, g_stack[g_stack_top]->Buffer, len);
        data->wstr[len] = L'\0';
        DDbgPrint("Answering with %ws\r\n", data->wstr);

        ExFreePool(g_stack[g_stack_top]);
      } else {
        DDbgPrint("Cannot put %d bytes into %d.\r\n", len, data->sz);
        DDbgPrint("File 2 pop: %ws\r\n", g_stack[g_stack_top]->Buffer);
        ++g_stack_top;
      }

      KeReleaseMutex(&g_mutex, FALSE);

      data->sz = len;
      break;

    default:
      DDbgPrint("Ignoring call with code %d\r\n", data->code);
      break;
    }
    irp->IoStatus.Status = STATUS_SUCCESS;
  }
    break;

  default: // STATUS_NOT_IMPLEMENTED takes action
    break;
  }

  EXIT_PROC(irp->IoStatus.Status);
}
#undef EXIT_PROC
