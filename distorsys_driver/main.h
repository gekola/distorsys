#ifndef __MAIN_H__
#define __MAIN_H__

#include <fltKernel.h>
#include <ntifs.h>

#include "device_naming.h"
#include "ioctl.h"

/******************************************************************************
 *  Prototypes
 ******************************************************************************/
NTSTATUS driver_entry(PDRIVER_OBJECT driver_obj, PUNICODE_STRING reg_path);
VOID driver_unload(PDRIVER_OBJECT driver_obj);


NTSTATUS filter_load(PCFLT_RELATED_OBJECTS flt_objs,
                     FLT_INSTANCE_SETUP_FLAGS  flags,
                     DEVICE_TYPE  vol_dev_type,
                     FLT_FILESYSTEM_TYPE  vol_fs_type);

NTSTATUS filter_unload(FLT_FILTER_UNLOAD_FLAGS flags);


NTSTATUS stub_dispatch(PDEVICE_OBJECT device_obj, PIRP irp);
NTSTATUS dispatch_dev_ctrl(PDEVICE_OBJECT device_obj, PIRP irp);
NTSTATUS dispatch_dev_create(PDEVICE_OBJECT device_obj, PIRP irp);

FLT_POSTOP_CALLBACK_STATUS post_file_op_clb(PFLT_CALLBACK_DATA data,
                                            PCFLT_RELATED_OBJECTS flt_objs,
                                            PVOID *completion_context,
                                            FLT_POST_OPERATION_FLAGS flags);

IO_WORKITEM_ROUTINE add_file;

/******************************************************************************
 *  Structures
 ******************************************************************************/
typedef struct _MY_WI_PARAM {
  PIO_WORKITEM workitem;
  PUNICODE_STRING filename;
} MY_WI_PARAM, *PMY_WI_PARAM;

/******************************************************************************
 *  Globals and consts
 ******************************************************************************/
extern PDEVICE_OBJECT g_dev_obj;
extern PDRIVER_OBJECT g_driver_object;
extern PFLT_FILTER g_filter;
extern KMUTEX g_mutex;

#define GSTACK_SIZE 256

extern INT g_stack_top;
extern PUNICODE_STRING g_stack[GSTACK_SIZE];
extern UNICODE_STRING g_prefix;

#ifndef RELEASE_VERSION
#define DEV_LIST_SIZE 128
#define DBG_PREF "distorsys filter: "
#define DDbgPrint(...) DbgPrint(DBG_PREF __VA_ARGS__)
#else
#define DDbgPrint(...)
#endif

#endif
