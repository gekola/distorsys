#include <exception>

#include "../distorsys_driver/device_naming.h"
#include "../distorsys_driver/ioctl.h"

#include "driver_interface.h"

DriverInterface &DriverInterface::getInstance()
{
  static DriverInterface instance;
  return instance;
}

DriverInterface::DriverInterface()
{
  file = CreateFileW(L"\\\\.\\" MY_DRIVER_NAME,
                     GENERIC_READ | GENERIC_WRITE, 0,
                     NULL, OPEN_EXISTING, 0, NULL);
  if (!file) {
    throw new std::exception("Cannot open device. Maybe driver is not loaded.");
  }
}

DriverInterface::~DriverInterface()
{
  CloseHandle(file);
}

bool DriverInterface::setPrefix(LPCWSTR prefix)
{
  DWORD bytes_returned;
  IOCTL_DATA my_data;

  my_data.code = MCODE_SET_PREF;
  my_data.sz = 0;
  my_data.wstr = const_cast<LPWSTR>(prefix);

  bool sucess = DeviceIoControl(file, D_CTL,
                                &my_data, sizeof(IOCTL_DATA),
                                &my_data, sizeof(IOCTL_DATA),
                                &bytes_returned, NULL);

  return sucess;
}


size_t DriverInterface::getNext(LPWSTR buf, size_t size)
{
  DWORD bytes_returned;
  IOCTL_DATA my_data;

  my_data.code = MCODE_GET;
  my_data.sz = (USHORT) size; // FIXME
  my_data.wstr = buf;

  DeviceIoControl(file, D_CTL,
                  &my_data, sizeof(IOCTL_DATA),
                  &my_data, sizeof(IOCTL_DATA),
                  &bytes_returned, NULL);

  return my_data.sz;
}
