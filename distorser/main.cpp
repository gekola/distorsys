#include <algorithm>
#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>
#include <mutex>
#include <vector>

#include <conio.h>

#include "driver_interface.h"
#include "md5.h"
#include "tcp_client.h"
#include "tcp_server.h"
#include "pipe_server.h"

#include "node.h"
#include "file.h"

using namespace std;

#define BUFSIZE 2048
#define LOCAL_PREFIX L"C:\\temp"
#define PREFIX L"\\Device\\HarddiskVolume2\\temp"
#define SLEEPTIME 5000

int wmain(int argc, wchar_t **argv)
{
  if (argc != 2)
    return -1;

  vector<Node *> *nodes = new vector<Node *>();
  vector<File *> *files = new vector<File *>();
  Node *self_node = new Node(argv[1], LOCAL_PREFIX);
  nodes->push_back(self_node);
  mutex *mut = new mutex();

  DriverInterface &di = DriverInterface::getInstance();
  di.setPrefix(PREFIX L"\\");
  TcpServer *ts = new TcpServer(L"27003", [mut, files, nodes](LPCWSTR req, size_t sz){
    wcout << req << L"\n";
    if (sz == 0) {
      return;
    } else if (req[0] == L'N') {
      LPCWSTR code = req + 1;

      size_t pos = 1;
      while (pos < sz && req[pos] != L'\0')
        ++pos;
      if (++pos >= sz)
        return;
      LPCWSTR path = req + pos;

      while(pos < sz && req[pos] != L'\0')
        ++pos;
      if (++pos >= sz)
        return;
      LPCWSTR hash = req + pos;

      wcout << L"N : " << code << L" : " << path << " : " << hash << L"\n";

      mut->lock();
      auto file = find_if(files->begin(), files->end(), [path](File *f){ return f->cmpPath(path); });
      auto node = find_if(nodes->begin(), nodes->end(), [code](Node *n){ return n->cmpCode(code); });
      if (node == nodes->end()) {
        mut->unlock();
        return;
      }

      if (file == files->end()) {
        files->push_back(new File(path, hash, code));
      } else {
        if ((*file)->cmpHash(hash)) {
          (*file)->addOwner(code);
        } else {
          (*file)->replaceOwner(code);
        }
      }

      mut->unlock();
    } else if (req[0] == L'R') {
      LPCWSTR code = req + 1;

      size_t pos = 1;
      while (pos < sz && req[pos] != L'\0')
        ++pos;
      if (++pos >= sz)
        return;
      LPCWSTR address = req + pos;

      while(pos < sz && req[pos] != L'\0')
        ++pos;
      if (++pos >= sz)
        return;
      LPCWSTR prefix = req + pos;

      wcout << L"R : " << code << L" : " << address << " : " << prefix << L"\n";

      mut->lock();
      nodes->push_back(new Node(code, prefix, address));
      mut->unlock();
    } else {
      wprintf(L"Invalid request: %s\n", req);
    }
  });

  PipeServer *ps = new PipeServer(L"\\\\.\\pipe\\distorser_pipe",
    [mut, files, nodes](LPWSTR req, DWORD *resp_sz) {
               wcout << "Req: \"" << req << L"\"\n";
               wstringstream ss;
               mut->lock();
               for (auto it = files->begin(); it != files->end(); ++it) {
                 ss << (*it)->getPath().c_str() << L"\n";
                 wstring code = (*it)->getOwners()->at(0);
                 Node *owner = *find_if(nodes->begin(), nodes->end(),
                   [&code](Node *node){ return node->cmpCode(code); });
                 ss << owner->getPrefix().c_str() << L"\n";
               }
               mut->unlock();
               ss << L"\n";
               size_t sz = ss.str().size();
               (*resp_sz) = (DWORD) (sz * sizeof(WCHAR));
               LPWSTR res = (LPWSTR) malloc((sz + 1) * sizeof(WCHAR));
               res[sz] = L'\0';
               wcsncpy(res, ss.str().c_str(), sz);
               wprintf(L"P %d: %s\n", sz, res);
               return res;
  });

  //_getch();
  //return 0;

  try {
    auto a = new TcpClient(L"othhost");
    auto res = wstring(L"R").append(argv[1]).
               append(L"\0", 1).append(L"othhost").append(L"\0", 1).
               append(L"\\\\").append(argv[1]).append(L"\\temp").
               append(L"\0R", 2);
    a->send(res.c_str(), res.size() * sizeof(wchar_t));
    delete a;
  } catch (...) {
    wprintf(L"Initial connection failed.\n");
  }

  for (;;) {
    WCHAR b[BUFSIZE] = L"\0";
    WCHAR md5[33]; md5[32] = L'\0';
    size_t sz = di.getNext(b, 2048);
    bool send_netw_not = false;
    if (sz == 0) {
      Sleep(SLEEPTIME);
      continue;
    }
    if (sz < BUFSIZE) {
      LPCWSTR path = b + (sizeof(PREFIX) - 1) / sizeof(WCHAR);
      wprintf(L"%ld: %s :", sz, path);
      mut->lock();

      DWORD res = getMd5((self_node->getPrefix() + path).c_str(), md5);
      wprintf(L" %s :", md5);
      if (res != 0) {
        mut->unlock();
        continue;
      }

      vector<File *>::iterator it = find_if(files->begin(), files->end(),
        [path](File *file) -> bool { return file->cmpPath(path); });
      if (it == files->end()) {
        files->push_back(new File(path, md5, self_node->getCode()));
        wprintf(L" new\n");
        send_netw_not = true;
      } else {
        if ((*it)->cmpHash(md5)) {
          (*it)->addOwner(self_node->getCode().c_str());
          wprintf(L" exists\n");
        } else {
          (*it)->replaceOwner(self_node->getCode().c_str());
          wprintf(L" changed\n");
          send_netw_not = true;
        }
      }

      mut->unlock();

      if (send_netw_not) {
        try {
          auto a = new TcpClient(L"othhost");
          auto res = wstring(L"N").append(argv[1]).
                     append(L"\0", 1).append(path).
                     append(L"\0", 1).append(md5).
                     append(L"\0L", 2);
          a->send(res.c_str(), res.size() * sizeof(wchar_t));
          delete a;
        } catch (...) {
        }
      }
    } else {
      wprintf(L"Insufficent buffer size (%d / %lu).\n", BUFSIZE, sz);
    }
  }

  delete ts;
  delete ps;
  return 0;
}
