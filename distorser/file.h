#ifndef __FILE_H__
#define __FILE_H__

#include <string>
#include <vector>

#include <Windows.h>

class File
{
public:
  File(std::wstring path, std::wstring hash, std::wstring owner);
  void addOwner(std::wstring owner);
  void replaceOwner(std::wstring owner);

  std::wstring getPath();
  bool cmpHash(std::wstring o_hash);
  bool cmpPath(std::wstring o_path);
  std::vector<std::wstring> *getOwners();

private:
  std::wstring path;
  std::wstring hash;
  std::vector<std::wstring> *owners;
};

#endif
