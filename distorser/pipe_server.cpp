#include <exception>

#include "pipe_server.h"

#define BUFSIZE 512

using namespace std;

PipeServer::PipeServer(LPCWSTR name, function<LPWSTR(LPWSTR, DWORD *)> clbk) :
  clbk(clbk), name(name), cont(true)
{
  listener_thread = new thread(PipeServer::listener, this);
}

void PipeServer::listener(PipeServer *self)
{
  bool connected = false;
  HANDLE pipe = INVALID_HANDLE_VALUE;

  while (self->cont) {
    wprintf(L"Pipe Server: Main thread awaiting client connection on %s\n", self->name);
    pipe = CreateNamedPipeW(
        self->name,               // pipe name
        PIPE_ACCESS_DUPLEX,       // read/write access
        PIPE_TYPE_MESSAGE |       // message type pipe
        PIPE_READMODE_MESSAGE |   // message-read mode
        PIPE_WAIT,                // blocking mode
        PIPE_UNLIMITED_INSTANCES, // max. instances
        BUFSIZE,                  // output buffer size
        BUFSIZE,                  // input buffer size
        0,                        // client time-out
        NULL);                    // default security attribute

    if (pipe == INVALID_HANDLE_VALUE)
      throw new exception("CreateNamedPipe failed.");

    connected = ConnectNamedPipe(pipe, NULL) || (GetLastError() == ERROR_PIPE_CONNECTED);

    if (connected) {
      wprintf(L"Pipe Server: Client connected, creating a processing thread.\n");

      new thread([pipe, self]() {
        WCHAR request[BUFSIZE];
        LPWSTR reply;

        DWORD read_bytes = 0, reply_bytes = 0, written_bytes = 0;
        BOOL success = FALSE;

        for(;;) { 
          success = ReadFile(pipe, request, BUFSIZE*sizeof(WCHAR),
                             &read_bytes, NULL);

          if (!success || read_bytes == 0) {
            if (GetLastError() == ERROR_BROKEN_PIPE) {
              wprintf(L"Pipe Server: InstanceThread: client disconnected.\n");
            } else {
              wprintf(L"Pipe Server: InstanceThread ReadFile failed, GLE=%d.\n", GetLastError());
            }
            break;
          }

          request[read_bytes/sizeof(WCHAR)] = L'\0';
          reply = self->clbk(request, &reply_bytes);

          success = WriteFile(pipe, reply, reply_bytes,
                               &written_bytes, NULL);

          if (!success || reply_bytes != written_bytes) {
            wprintf(L"Pipe Server: InstanceThread: WriteFile failed, GLE=%d.\n", GetLastError());
            free(reply);
            break;
          }
          free(reply);
        }

        FlushFileBuffers(pipe);
        DisconnectNamedPipe(pipe);
        CloseHandle(pipe);

        printf("Pipe Server: InstanceThread exitting.\n");
      });
    } else {
      CloseHandle(pipe); 
    }
  } 
}

PipeServer::~PipeServer()
{
  stop();
  delete listener_thread;
}

void PipeServer::stop()
{
  cont = false;
  listener_thread->join();
}
