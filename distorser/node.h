#ifndef __NODE_H__
#define __NODE_H__

#include <string>

class Node
{
public:
  Node(std::wstring code, std::wstring prefix, std::wstring address=L"");

  bool cmpCode(std::wstring o_code);
  std::wstring getPrefix();
  std::wstring getCode();

private:
  std::wstring address;
  std::wstring prefix;
  std::wstring code;
};

#endif
