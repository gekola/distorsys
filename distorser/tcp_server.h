#ifndef __TCP_SERVER_H__
#define __TCP_SERVER_H__

#include <thread>

#include <winsock2.h>

typedef enum {
  PT_DUMMY = 0,

  PT_REGISTER,
  PT_PARAM,

  PT_QUIT_NOTIFICATION
} PacketCommand;

typedef struct Packet {
  PacketCommand cmd;
  union {
    
  } data;
} Packet;

class TcpServer
{
public:
  TcpServer(LPCWSTR port=L"27003",
            std::function<void(LPCWSTR, size_t)> clbk=[](LPCWSTR, size_t){});
  ~TcpServer();

private:
  static void listener(TcpServer *self);

  void stop();

  SOCKET listen_socket;
  bool cont;
  std::thread *listener_thread;
  std::function<void(LPCWSTR, size_t)> clbk;
};

#endif
