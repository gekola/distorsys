#include <stdio.h>
#include <windows.h>

#include "../distorsys_driver/device_naming.h"
#include "../distorsys_driver/ioctl.h"

int set_prefix(LPWSTR prefix)
{
  HANDLE hFile;
  DWORD bytes_returned;
  IOCTL_DATA my_data = {MCODE_SET_PREF, 0, prefix};
  hFile = CreateFileW(L"\\\\.\\" MY_DRIVER_NAME,
                      GENERIC_READ | GENERIC_WRITE, 0,
                      NULL, OPEN_EXISTING, 0, NULL);

  if (!hFile) {
    DWORD err = GetLastError();
    printf("Sorry, fail while opening device io control.\n");
    printf("Error: %u\n", err);
    return -1;
  }

  if (!DeviceIoControl(hFile, D_CTL, &my_data,
                       sizeof(IOCTL_DATA),
                       &my_data,
                       sizeof(IOCTL_DATA),
                       &bytes_returned, NULL)) {
    DWORD err = GetLastError();
    printf("Failed to access device io control.\n");
    printf("Error: %u = \"%s\"\n", err);

    CloseHandle(hFile);
    return -2;
  }


  while(getch() != 'q') {
    WCHAR b[2048] = L"\0";
    my_data.code = MCODE_GET;
    my_data.sz = 2048;
    my_data.wstr = b;
    DeviceIoControl(hFile, D_CTL, &my_data,
                    sizeof(IOCTL_DATA),
                    &my_data,
                    sizeof(IOCTL_DATA),
                    &bytes_returned, NULL);
    wprintf(L": %s\n", my_data.wstr);
  }


  CloseHandle(hFile);
}

int main(int argc, char **argv)
{
  return set_prefix(L"\\Device\\HarddiskVolume2\\temp\\");
}
