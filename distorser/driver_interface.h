#ifndef __DRIVER_INTERFACE_H__
#define __DRIVER_INTERFACE_H__

#include <windows.h>


class DriverInterface
{
public:
  static DriverInterface &getInstance();

  bool setPrefix(LPCWSTR prefix);
  size_t getNext(LPWSTR buf, size_t size);

private:
  DriverInterface();
  ~DriverInterface();

  DriverInterface(DriverInterface const &); // Not implemented
  void operator=(DriverInterface const &);  // Not implemented

  HANDLE file;
};

#endif
