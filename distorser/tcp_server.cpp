#include <exception>

#include <windows.h>
#include <ws2tcpip.h>

#include "tcp_server.h"

#define BUFSIZE 512

#pragma comment (lib, "Ws2_32.lib")

using namespace std;

TcpServer::TcpServer(LPCWSTR port, function<void(LPCWSTR, size_t)> clbk) :
  cont(true), clbk(clbk)
{
  WSADATA wsa_data;
  int status;

  ADDRINFOW hints, *result;

  status = WSAStartup(MAKEWORD(2,2), &wsa_data);
  if (status != 0) {
    throw new std::exception("TCP Server: WSAStartup failed");
  }

  ZeroMemory(&hints, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;
  hints.ai_flags = AI_PASSIVE;

  status = GetAddrInfoW(NULL, port, &hints, &result);
  if (status) {
    WSACleanup();
    throw new std::exception("TCP Server: getaddrinfo failed");
  }

  listen_socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
  if (listen_socket == INVALID_SOCKET) {
    FreeAddrInfoW(result);
    WSACleanup();
    throw new std::exception("TCP Server: socket failed");
  }

  status = ::bind(listen_socket, result->ai_addr, (int)result->ai_addrlen);
  if (status == SOCKET_ERROR) {
    FreeAddrInfoW(result);
    closesocket(listen_socket);
    WSACleanup();
    throw new std::exception("TCP Server: bind failed");
  }

  FreeAddrInfoW(result);

  status = listen(listen_socket, SOMAXCONN);
  if (status == SOCKET_ERROR) {
    closesocket(listen_socket);
    WSACleanup();
    throw new std::exception("TCP Server: listen failed");
  }

  listener_thread = new std::thread(listener, this);
}

TcpServer::~TcpServer()
{
  stop();
  delete listener_thread;
  closesocket(listen_socket);
}

void TcpServer::stop()
{
  cont = false;
  listener_thread->join();
}

void TcpServer::listener(TcpServer *self)
{
  while (self->cont) {
    SOCKET socket = accept(self->listen_socket, NULL, NULL);
    if (socket != INVALID_SOCKET)
      new std::thread([socket, &self]{
        int iResult;
        do {
          char recvbuf[BUFSIZE];
          iResult = recv(socket, recvbuf, 512, 0);
          if (iResult > 0) {
            printf("TCP Server: Bytes received: %d\n", iResult);
            self->clbk((LPCWSTR)recvbuf, iResult);
          } else if (iResult == 0)
            printf("TCP Server: Connection closing...\n");
          else  {
            printf("TCP Server: recv failed with error: %d\n", WSAGetLastError());
            closesocket(socket);
            return;
          }
        } while (iResult > 0);
        iResult = shutdown(socket, SD_SEND);
        if (iResult == SOCKET_ERROR) {
          printf("TCP Server: shutdown failed with error: %d\n", WSAGetLastError());
        }
        closesocket(socket);
      });
  }
}
