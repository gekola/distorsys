#include "node.h"

using namespace std;

Node::Node(wstring code, wstring prefix, wstring address) :
  code(code), prefix(prefix), address(address)
{
}

bool Node::cmpCode(wstring o_code)
{
  return code.compare(o_code) == 0;
}

wstring Node::getCode()
{
  return code;
}

wstring Node::getPrefix()
{
  return prefix;
}