#include "file.h"

using namespace std;

File::File(wstring path, wstring hash, wstring owner) :
  path(path), hash(hash)
{
  owners = new vector<wstring>();
  owners->push_back(owner);
}

void File::addOwner(wstring owner)
{
  owners->push_back(owner);
}

void File::replaceOwner(wstring owner)
{
  owners->clear();
  addOwner(owner);
}

bool File::cmpHash(std::wstring o_hash)
{
  return hash.compare(o_hash) == 0;
}

bool File::cmpPath(std::wstring o_path)
{
  return path.compare(o_path) == 0;
}

wstring File::getPath()
{
  return path;
}

vector<wstring> *File::getOwners()
{
  return owners;
}
