#include <exception>

#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include "tcp_client.h"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define BUFSIZE 512

using namespace std;

TcpClient::TcpClient(LPCWSTR addr, LPCWSTR port) :
  con_socket(INVALID_SOCKET)
{
    WSADATA wsa_data;
    struct addrinfoW *result = NULL,
                     *ptr = NULL,
                     hints;
    int status;
    
    status = WSAStartup(MAKEWORD(2,2), &wsa_data);
    if (status != 0) {
        throw new exception("WSAStartup failed");
    }

    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    status = GetAddrInfoW(addr, port, &hints, &result);
    if ( status != 0 ) {
        WSACleanup();
        throw new exception("getaddrinfo failed");
    }

    for(ptr=result; ptr != NULL; ptr = ptr->ai_next) {

        con_socket = socket(ptr->ai_family, ptr->ai_socktype, 
            ptr->ai_protocol);
        if (con_socket == INVALID_SOCKET) {
            WSACleanup();
            throw new exception("socket failed");
        }

        status = connect( con_socket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (status == SOCKET_ERROR) {
            closesocket(con_socket);
            con_socket = INVALID_SOCKET;
            continue;
        }
        break;
    }

    FreeAddrInfoW(result);

    if (con_socket == INVALID_SOCKET) {
        WSACleanup();
        throw new exception("Unable to connect to server");
    }
}
void TcpClient::send(LPCWSTR sendbuf, size_t size)
{
    char recvbuf[BUFSIZE];
    DWORD status = ::send(con_socket, (LPCSTR)sendbuf, size, 0);
    if (status == SOCKET_ERROR) {
        printf("TCP Client: send failed with error: %d\n", WSAGetLastError());
        closesocket(con_socket);
        WSACleanup();
        throw new exception();
    }

    printf("TCP Client: Bytes Sent: %ld\n", status);

    status = shutdown(con_socket, SD_SEND);
    if (status == SOCKET_ERROR) {
        printf("TCP Client: shutdown failed with error: %d\n", WSAGetLastError());
    }

    do {
      status = recv(con_socket, recvbuf, BUFSIZE, 0);
      if (status > 0)
        printf("TCP Client: Bytes received: %d\n", status);
      else if ( status == 0 )
        printf("TCP Client: Connection closed\n");
      else
        printf("TCP Client: recv failed with error: %d\n", WSAGetLastError());
    } while(status > 0);
}

TcpClient::~TcpClient()
{
    closesocket(con_socket);
    WSACleanup();
}
