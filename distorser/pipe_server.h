#ifndef __PIPE_SERVER_H__
#define __PIPE_SERVER_H__

#include <thread>

#include <windows.h>

typedef LPCWSTR (pipe_serv_clbk)(LPCWSTR req, DWORD &resp_sz);

class PipeServer
{
public:
  PipeServer(LPCWSTR name = L"\\\\.\\pipe\\distorser_pipe",
             std::function<LPWSTR(LPWSTR, DWORD *)> clbk =
             [](LPWSTR req, DWORD *resp_sz) {
               (*resp_sz) = (DWORD) wcslen(req) * sizeof(WCHAR);
               LPWSTR res = (LPWSTR) malloc(*resp_sz + sizeof(WCHAR));
               res[*resp_sz / sizeof(WCHAR)] = L'\0';
               wprintf(L"%s\n", res);
               wcsncpy(res, req, *resp_sz / sizeof(WCHAR));
               return res;
  });
  ~PipeServer();

private:
  void stop();
  static void listener(PipeServer *self);
  
  std::thread *listener_thread;
  std::function<LPWSTR(LPWSTR, DWORD *)> clbk;
  LPCWSTR name;
  bool cont;
};

#endif
