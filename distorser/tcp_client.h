#ifndef __TCP_CLIENT_H__
#define __TCP_CLIENT_H__

#include <windows.h>
#include <winsock2.h>


class TcpClient
{
public:
  TcpClient(LPCWSTR addr, LPCWSTR port = L"27003");
  ~TcpClient();

  void send(LPCWSTR sendbuf, size_t size);

private:
  SOCKET con_socket;
};

#endif
